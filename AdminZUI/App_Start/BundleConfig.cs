﻿using System.Web;
using System.Web.Optimization;

namespace AdminZUI
{
    public class BundleConfig
    {
        // 有关 Bundling 的详细信息，请访问 http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/JS/Jquery").Include(
                        "~/Resources/Jquery/jquery-{version}.js",//jquery-{version}.min.js
                        "~/Resources/Jquery/jquery.pjax.js"));

            bundles.Add(new ScriptBundle("~/JS/ZUI").Include(
                        "~/Resources/ZUI/js/zui.js",
                        "~/Resources/ZUI/js/main.js"));


            bundles.Add(new StyleBundle("~/Resources/ZUI").Include(
                "~/Resources/ZUI/css/zui.css",
                "~/Resources/ZUI/css/main.css"));

        }
    }
}