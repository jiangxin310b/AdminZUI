﻿/// <reference path="../../Jquery/jquery-2.1.4.js" />
/// <reference path="../../Jquery/jquery.pjax.js" />

var left_right_times;
$(function () {
    _zuimain.init_menu();
});

//
var _zuimain = {
    top_menu_hover: function () {
        $(".page-wapper-top-ul li").mouseover(function () {
            $(this).find("i").css("display", "block").stop().animate({ opacity: 1 }, 200);
        }).mouseleave(function () {
            $(this).find("i").stop().animate({ opacity: 0 }, 200, function () {
                $(this).css("display", "none");
            });
        });
    },
    init_menu: function () {
        //页面加载设置皮肤
        _zuimain.set_top_bg(localStorage.getItem("bg-color") ? localStorage.getItem("bg-color") : "#324157");
        //给菜单添加事件
        // 手动通过点击模拟高亮菜单项
        $('#treeMenu').on('click', 'a', function () {
            $('#treeMenu li.active').removeClass('active');
            $(this).closest('li').addClass('active');

            var $this = $(this);
            var href = $(this).attr("data-href");

            if (!href) return false;
            _zuimain._pjax({
                dom: '#PageContent',
                href: href,
            });
            var text = $this.text();
            //获取 点击菜单的信息 切换tab页面
            var tab = $(".page-wapper-top-ul");
            if (tab) {
                var iscz = tab.find('li[data-href="' + href + '"]');
                $("ul.page-wapper-top-ul li").removeAttr("class");
                if (iscz.length > 0) {
                    iscz.addClass("page-select");
                    $(iscz).css("width", $(iscz).outerWidth(true) + 1);
                } else {
                    $("ul.page-wapper-top-ul li").removeAttr("class");
                    //<li data-href="/Home/Home/?tag=哈喽！22222222">会员管理3<i class="icon-times" onclick="$(this).parent().remove(); return false;"></i></li>
                    var html = "";
                    html += '<li class="page-select" data-href="' + href + '" onclick="_zuimain.top_menu(this);">';
                    html += "<span>" + text + "</span>";
                    html += '<i class="icon-times" onclick="_zuimain.close_tab(event,this);"></i>';
                    html += '</li>';
                    tab.append(html);
                    $(".page-select").css("width", $(".page-select").outerWidth(true) + 1);
                }
                _zuimain.top_menu_hover();
                //点了标签去定位
                _zuimain.top_menu_location(iscz);
            }
        });
        //菜单关闭开启设置
        _zuimain.togo_menu();
        //设置皮肤的click 事件
        _zuimain.set_pifu();
        _zuimain.load_default_page();
        $(".page-wapper-top-right").unbind("click").click(function (e) {
            e.stopPropagation();
            e.preventDefault();
            clearTimeout(left_right_times);//阻止重复点击
            left_right_times = setTimeout(function () {
                _zuimain.top_menu_next();
            }, 200);
        });
        $(".page-wapper-top-left").unbind("click").click(function (e) {
            e.stopPropagation();
            e.preventDefault();
            clearTimeout(left_right_times);//阻止重复点击
            left_right_times = setTimeout(function () {
                _zuimain.top_menu_prev();
            }, 200);
        });
    },
    togo_menu: function () {
        //菜单打开和关闭
        var width = window.innerWidth;
        $(".page-wapper-top-left1").click(function () {
            var left_menu = $(".left-menu");
            if (left_menu.css("left") == '-200px') {
                //显示
                left_menu.stop().animate({ left: '0px' }, 200);
                if (width <= 768)
                    $(".page-wapper-content").css({ width: $(".page-wapper-content").outerWidth(true) - 200 });
                $(".page-wapper-content").stop().animate({ margin: "0 0 0 200px" }, 200);

            } else {
                left_menu.stop().animate({ left: '-200px' }, 200);
                $(".page-wapper-content").stop().animate({ margin: "0" }, 200, function () {
                    if (width <= 768)
                        $(".page-wapper-content").css({ width: "100%" });
                });
            }
        });
        //自适应        
        if (width <= 768) {//小屏幕自动隐藏菜单
            $(".page-wapper-top-left1").click();
        }
        $(window).resize(function () {
            if (width <= 768) {//小屏幕自动隐藏菜单
                $(".page-wapper-top-left1").click();
            }
        });
    },
    top_menu: function ($this) {
        $this = $($this);
        if ($this.is("li")) {
            //判断是往前面移动。还是往后面移动
            if ($this.index() > $("ul.page-wapper-top-ul li[class='page-select']").index()) {
                _zuimain.top_menu_next();
            } else if ($this.index() < $("ul.page-wapper-top-ul li[class='page-select']").index()) {
                _zuimain.top_menu_prev();
            }

            $("ul.page-wapper-top-ul li").removeAttr("class");
            $this.addClass("page-select");
            var href = $this.attr("data-href");
            if (href) {
                _zuimain._pjax({
                    dom: '#PageContent',
                    href: href,
                });
            }
            //点击顶部也要对左侧做样式
            $('#treeMenu li.active').removeClass('active');
            $("#treeMenu a[data-href='" + href + "']").closest('li').addClass('active');
        }
    },
    _pjax: function (options) {//pjax
        var defaults = {
            href: "",
            dom: "",
            filter: null,
            callback: null,
        };
        var options = $.extend(defaults, options);
        $.pjax({
            //selector: 'a',
            url: options.href,
            container: options.dom, //内容替换的容器
            show: 'fade',  //展现的动画，支持默认和fade, 可以自定义动画方式，这里为自定义的function即可。
            cache: true,  //是否使用缓存
            storage: true,  //是否使用本地存储
            titleSuffix: '', //标题后缀
            filter: function (href) {
                if (options.filter != null) {
                    options.filter(href);
                }
            },
            callback: function (status) {
                if (options.callback != null) {
                    options.callback(status);
                } else {
                    var type = status.type;
                    switch (type) {
                        case 'success':;

                            break; //正常
                        case 'cache':;

                            break; //读取缓存 
                        case 'error':;

                            break; //发生异常
                        case 'hash':;

                            break; //只是hash变化
                    }
                }
            }
        });
    },
    set_pifu: function () {//设置皮肤
        $(".pifu-li").click(function () {
            var color = $(this).css("background-color");
            _zuimain.set_top_bg(color);
            localStorage.setItem("bg-color", color);//将颜色存入 本地存储
        });
    },
    set_top_bg: function (color) {//设置背景色
        $(".page-wapper-top-left").css("background-color", color);
        $(".page-wapper-top-left1").css("background-color", color);
        $(".page-wapper-top-left2").css("background-color", color);
        $(".page-wapper-top").css("background-color", color);
        $(".page-wapper-top-right").css("background-color", color);
        $(".page-wapper-top-right1").css("background-color", color);
    },
    close_tab: function (event, $this) {
        $this = $($this);
        _zuimain.stopDefaultEvent(event);
        _zuimain.stopEventBubble();
        //关闭选项卡
        if ($this.parent().hasClass("page-select")) {//判断移除的是否是选中的选项卡
            $("#PageContent").html("");
            //
            $("ul.page-wapper-top-ul li").removeAttr("class");
            var li = $this.parent();
            var next = li.next();
            var prev = li.prev();
            console.log(prev);
            if (next.length > 0) {
                next.addClass("page-select");
                $("#treeMenu a[data-href='" + next.attr("data-href") + "']").click();
            } else {
                if (prev.length > 0) {
                    prev.addClass("page-select");
                    $("#treeMenu a[data-href='" + prev.attr("data-href") + "']").click();
                }
            }
        }
        $this.parent().remove();
    },
    close_all_tab: function () {
        //关闭所有标签
        var tab = $(".page-wapper-top-ul li");
        $.each(tab, function () {
            var home = $(this).attr('data-default');
            if (home) {
                _zuimain.load_default_page();
            } else {
                $(this).remove();
            }
        });
    },
    load_default_page: function () {//加载默认页面
        $("#treeMenu a[data-default='home']").click();//页面首次加载默认的页面
    },
    stopDefaultEvent: function (e) {//禁止默认事件
        //阻止默认浏览器动作(W3C) 
        if (e && e.preventDefault)
            e.preventDefault();
            //IE中阻止函数器默认动作的方式
        else
            window.event.returnValue = false;
        return false;
    },
    stopEventBubble: function () {//禁止事件冒泡
        var oEvent = arguments.callee.caller.arguments[0] || event;
        oEvent.cancelBubble = true;
    },
    top_menu_next: function () {
        //判断最后一个li距离右边的像素位置 
        var last_li = $(".page-wapper-top-ul li:last").offset().left;
        var width = $(".page-wapper-top-ul li:last").outerWidth(true);
        var left = $(".page-wapper-top-right").offset().left;
        if ((last_li + width) <= left) {
            $(".page-wapper-top-ul").stop();
            return false;
        }

        $(".page-wapper-top-ul").stop().animate({
            "left": "-=200px"
        }, "fast", function () {
            last_li = $(".page-wapper-top-ul li:last").offset().left;
            width = $(".page-wapper-top-ul li:last").outerWidth(true);
            left = $(".page-wapper-top-right").offset().left;
            if ((last_li + width) <= left) {
                $(".page-wapper-top-ul").stop();
                return false;
            }
        });
    },
    top_menu_prev: function () {
        //判断最后一个li距离右边的像素位置        
        if ($(".page-wapper-top-ul li:first").offset().left < 240) {
            var left = $(".page-wapper-top-ul").offset().left;
            if (left >= 240) {
                $(".page-wapper-top-ul").stop();
                return false;
            }
            $(".page-wapper-top-ul").stop().animate({
                "left": "+=200px"
            }, "fast", function () {
                left = $(".page-wapper-top-ul").offset().left;
                if (left >= 240) {
                    $(".page-wapper-top-ul").stop();
                    return false;
                }
            });
        }

    },
    top_menu_location: function () {//点了左边菜单 去 定位 顶部的 选项
        var top_menu_times = setInterval(function () {
            var page_select = $(".page-wapper-top-ul li[class='page-select']");
            var left = $(".page-wapper-top-right").offset().left;

            if (page_select.offset().left >= 240 && (page_select.offset().left) <= left) {
                $(".page-wapper-top-ul").stop();
                clearInterval(top_menu_times);
            }
            if ((page_select.offset().left + page_select.outerWidth(true)) >= left) {
                _zuimain.top_menu_next();
            } else if (page_select.offset().left < 240) {
                _zuimain.top_menu_prev();
            }
        }, 350);
    },
    pjax_events: function () {
        //监听pjax 请求完成
        $(document).on('pjax:success', function () {

        });
        $(document).on('pjax:send', function () {
            console.log("开始了！");
            // $('#loading').show()
        });
        $(document).on('pjax:complete', function () {
            setTimeout(function () {
                console.log("遮罩层结束了！");
            }, 1000);
            //$('#loading').hide()
        });
        $(document).on('pjax:timeout', function (event) {
            // Prevent default timeout redirection behavior
            event.preventDefault()
        })
        $(document).on('ready pjax:end', function (event) {
            $(event.target).initializeMyPlugin()
        })
    }
};

//全屏
var App = function () {
    var isFullScreen = false;
    var requestFullScreen = function () {//全屏
        var de = document.documentElement;
        if (de.requestFullscreen) {
            de.requestFullscreen();
        } else if (de.mozRequestFullScreen) {
            de.mozRequestFullScreen();
        } else if (de.webkitRequestFullScreen) {
            de.webkitRequestFullScreen();
        }
        else {
            alert("该浏览器不支持全屏");
        }
    };

    //退出全屏 判断浏览器种类
    var exitFull = function () {
        // 判断各种浏览器，找到正确的方法
        var exitMethod = document.exitFullscreen || //W3C
            document.mozCancelFullScreen ||    //Chrome等
            document.webkitExitFullscreen || //FireFox
            document.webkitExitFullscreen; //IE11
        if (exitMethod) {
            exitMethod.call(document);
        }
        else if (typeof window.ActiveXObject !== "undefined") {//for Internet Explorer
            var wscript = new ActiveXObject("WScript.Shell");
            if (wscript !== null) {
                wscript.SendKeys("{F11}");
            }
        }
    };

    return {
        handleFullScreen: function () {
            if (isFullScreen) {
                exitFull();
                isFullScreen = false;
            } else {
                requestFullScreen();
                isFullScreen = true;
            }
        },
    };

}();

