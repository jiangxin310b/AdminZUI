﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AdminZUI.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Home(string tag, string _pjax)
        {
            ViewData["tag"] = tag;
            if (string.IsNullOrEmpty(_pjax))
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

    }
}
